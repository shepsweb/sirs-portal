<?php

namespace Sirs\SirsPortal;

use Sirs\SirsPortal\Models\ApiUser;
use Faker\Generator as Faker;

$factory->define(ApiUser::class, function (Faker $faker) {
    return [
        'email' => $faker->unique()->email,
    ];
});
