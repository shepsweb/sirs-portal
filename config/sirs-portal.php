<?php

/*
 * You can place your custom package configuration in here.
 */
return [

    'home' => env('SIRS_PORTAL_HOME'),

    'allowed' => [

        'referers' => explode(',', env('SIRS_PORTAL_REFERERS', '')),

        'paths' => [],

    ],

    'provider' => [

        'name' => 'portal',

        'options' => [
            'driver' => 'eloquent',
            'model' => \Sirs\SirsPortal\Models\ApiUser::class,
        ],
    ],

    'abilities' => [

        // 'ability-name' => 'Ability Label',

    ],

];
