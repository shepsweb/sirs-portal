<?php

namespace Sirs\SirsPortal\Tests;

use Illuminate\Support\Facades\Route;
use Sirs\SirsPortal\Facades\SirsPortal;
use Orchestra\Testbench\TestCase as BaseTestCase;

class TestCase extends BaseTestCase
{
    protected $env = [];

    protected function getPackageProviders($app)
    {
        return [
            \Laravel\Sanctum\SanctumServiceProvider::class,
            \Sirs\SirsPortal\SirsPortalServiceProvider::class,
        ];
    }

    protected function getEnvironmentSetUp($app)
    {
        $envFilePath = __DIR__ . "/../.env.{$app->environment()}.php";

        if (file_exists($envFilePath)) {
            $this->env = require($envFilePath);

            if (isset($this->env['database'])) {
                $app['config']->set('database.default', 'testbench');
                $app['config']->set('database.connections.testbench', $this->env['database']);
            }
        }
    }

    protected function setUp(): void
    {
        parent::setUp();

        Route::middleware('web')->group(function () {
            SirsPortal::tokenRoutes();
        });
    }
}
