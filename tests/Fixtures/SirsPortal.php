<?php

namespace Sirs\SirsPortal\Tests\Fixtures;

use Sirs\SirsPortal\SirsPortal as BaseSirsPortal;

class SirsPortal extends BaseSirsPortal
{
    protected $abilities = [];

    /**
     * Get abilities from config
     *
     * @return array
     */
    public function abilities(): array
    {
        return $this->abilities;
    }

    /**
     * Set an ability
     *
     * @param string $key
     * @param string $value
     * @return self
     */
    public function setAbility(string $key, string $value): self
    {
        $this->abilities[$key] = $value;

        return $this;
    }
}
