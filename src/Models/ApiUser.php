<?php

namespace Sirs\SirsPortal\Models;

use Illuminate\Support\Str;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Foundation\Auth\User as Authenticatable;

class ApiUser extends Authenticatable
{
    use HasApiTokens;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Get token abilities
     *
     * @return string
     */
    public function abilities(): string
    {
        if ($token = $this->tokens->first()) {
            return collect($token->abilities)
                ->map(function ($ability) {
                    if ($ability === '*') {
                        return 'All';
                    }

                    return Str::title(str_replace('_', ' ', $ability));
                })
                ->implode(', ');
        }

        return '';
    }
}
