<?php

namespace Sirs\SirsPortal;

use Illuminate\Contracts\Http\Kernel;
use Illuminate\Support\ServiceProvider;
use Sirs\SirsPortal\Middleware\AuthenticateToken;

class SirsPortalServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        /*
         * Optional methods to load your package assets
         */
        // $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'sirs-portal');
        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'sirs-portal');
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');
        $this->loadFactoriesFrom(__DIR__ . '/../database/factories');
        // $this->loadRoutesFrom(__DIR__ . '/../routes/routes.php');

        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__ . '/../config/sirs-portal.php' => config_path('sirs-portal.php'),
            ], 'sirs-portal-config');

            // Publishing the views.
            $this->publishes([
                __DIR__ . '/../resources/views' => resource_path('views/vendor/sirs-portal'),
            ], 'sirs-portal-views');

            // Publishing assets.
            $this->publishes([
                __DIR__ . '/../resources/sirs-portal-ui/dist/sirs-portal.min.js' => public_path('vendor/sirs-portal-ui/sirs-portal.min.js'),
            ], 'sirs-portal-assets');

            // Publishing the translation files.
            /*$this->publishes([
                __DIR__.'/../resources/lang' => resource_path('lang/vendor/sirs-portal'),
            ], 'lang');*/

            // Registering package commands.
            // $this->commands([]);
        }
    }

    /**
     * Register the application services.
     */
    public function register()
    {
        // Automatically apply the package configuration
        $this->mergeConfigFrom(__DIR__ . '/../config/sirs-portal.php', 'sirs-portal');

        $this->setProviderConfig();

        // Register the main class to use with the facade
        $this->app->singleton('sirs-portal', function () {
            return new SirsPortal;
        });

        $this->configureMiddleware();
    }

    /**
     * Set provider config
     *
     * @return void
     */
    protected function setProviderConfig(): void
    {
        $name = config('sirs-portal.provider.name');

        config([
            "auth.providers.{$name}" => array_merge(
                config('sirs-portal.provider.options'),
                config("auth.providers.{$name}", [])
            ),
        ]);
    }

    /**
     * Configure the SirsPortal middleware and priority.
     *
     * @return void
     */
    protected function configureMiddleware(): void
    {
        $kernel = $this->app->make(Kernel::class);

        $kernel->pushMiddleware(AuthenticateToken::class)
            ->prependToMiddlewarePriority(AuthenticateToken::class);
    }
}
