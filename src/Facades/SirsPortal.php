<?php

namespace Sirs\SirsPortal\Facades;

use Illuminate\Support\Facades\Facade;
use Sirs\SirsPortal\Tests\Fixtures\SirsPortal as TestSirsPortal;

/**
 * @method static void setAuth($user = null)
 * @method static mixed getAuth()
 * @method static bool canLoadPortalView(?string $path = null)
 * @method static void tokenRoutes()
 * @method static void portalRoutes(?Closure $callback = null)
 * @method static bool isValidReferer(?string $referer)
 * @method static bool isAllowed(string $url)
 * @method static array abilities()
 *
 * @see \Sirs\SirsPortal\SirsPortal
 */
class SirsPortal extends Facade
{
    /**
     * Fake the instance for testing
     *
     * @return \Sirs\SirsPortal\Tests\Fixtures\SirsPortal
     */
    public static function fake(): TestSirsPortal
    {
        static::swap($fake = new TestSirsPortal);

        return $fake;
    }

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'sirs-portal';
    }
}
