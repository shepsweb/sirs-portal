<?php

namespace Sirs\SirsPortal\Middleware;

use Closure;
use Illuminate\Http\Request;
use Sirs\SirsPortal\Facades\SirsPortal;
use Sirs\SirsPortal\Exceptions\InvalidTokenException;

class AuthenticatePortalRoutes
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (SirsPortal::canLoadPortalView($request->getRequestUri())) {
            return $next($request);
        }

        throw new InvalidTokenException;
    }
}
