<?php

namespace Sirs\SirsPortal\Middleware;

use Closure;
use Laravel\Sanctum\Guard;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Auth\AuthManager;
use Illuminate\Support\Facades\Config;
use Sirs\SirsPortal\Facades\SirsPortal;
use Sirs\SirsPortal\Exceptions\InvalidTokenException;

class AuthenticateToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if ($this->allowAccess($request)) {
            return $next($request);
        }

        throw new InvalidTokenException;
    }

    /**
     * Allow access to the given request
     *
     * @param \Illuminate\Http\Request $request
     * @return bool
     */
    protected function allowAccess(Request $request): bool
    {
        if ((bool) $request->portal) {
            $referer = $request->header('Referer', null);
            return SirsPortal::isValidReferer($referer) && $this->isAuthenticated($request);
        }

        return true;
    }

    /**
     * Check if authenticated
     *
     * @param \Illuminate\Http\Request $request
     * @return bool
     */
    protected function isAuthenticated(Request $request): bool
    {
        if ($request->token) {
            $request->headers->set('Authorization', "Bearer {$request->token}");
        }

        $auth = resolve(AuthManager::class);

        $guard = new Guard(
            $auth,
            Config::get('sirs-portal.expiration'),
            Config::get('sirs-portal.provider.name')
        );

        SirsPortal::setAuth($guard->__invoke($request));

        return SirsPortal::canLoadPortalView($request->getRequestUri());
    }
}
