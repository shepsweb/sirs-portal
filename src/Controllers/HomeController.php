<?php

namespace Sirs\SirsPortal\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class HomeController
{
    public function home(Request $request)
    {
        if ($url = Config::get('sirs-portal.home')) {
            return redirect(portal_url($url));
        }

        return view('sirs-portal::home');
    }
}
