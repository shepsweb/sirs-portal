<?php

namespace Sirs\SirsPortal\Controllers;

use Illuminate\Http\Request;
use Sirs\SirsPortal\Models\ApiUser;
use Sirs\SirsPortal\Facades\SirsPortal;

class TokenManagerController
{
    public function index()
    {
        $users = ApiUser::with('tokens')->latest()->get();

        return view('sirs-portal::tokens.index', [
            'users' => $users,
            'abilities' => SirsPortal::abilities(),
        ]);
    }

    public function store(Request $request)
    {
        $validated = $request->validate([
            'email' => 'required|email|unique:api_users',
            'scopes' => 'required|array',
        ]);

        $user = ApiUser::create(['email' => $validated['email']]);

        $token = $user->createToken(config('app.url'), $request->get('scopes', []))->plainTextToken;

        $request->session()->flash('access_token', $token);
        $request->session()->flash('email', $user->email);

        return redirect()->route('sirs-portal.tokens.index');
    }

    public function destroy(Request $request, ApiUser $user)
    {
        $user->tokens()->delete();
        $user->delete();

        $request->session()->flash('success', 'Successfully revoked user');
        return redirect()->route('sirs-portal.tokens.index');
    }
}
